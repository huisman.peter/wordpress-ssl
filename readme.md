### swapfile 

`sudo fallocate -l 4G /swapfile`

`sudo chmod 600 /swapfile`

`sudo mkswap /swapfile`

`sudo swapon /swapfile`

Add swapfile to `/etc/fstab`

```
/swapfile none swap sw 0 0
```

### Install docker

`curl https://get.docker.com > install-docker.sh`

`chmod 755 install-docker.sh` 

`sudo usermod -aG docker huisman_peter`

